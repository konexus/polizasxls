﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolizasXLS
{
    public class PolizaExcelDM
    {
        public int Compania { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Debito { get; set; }
        public decimal Credito { get; set; }
        public bool NetoFiscal { get; set; }
        public decimal NetoFiscalDebito { get; set; }
        public decimal NetoFiscalCredito { get; set; }
        public bool Bandera { get; set; }
        public string Mesaje { get; set; }
        public List<string> NombresArchivosDentroZip { get; set; }

        /****************************************************/
        private string _NombreCompania;
        public string NombreCompania { get { return _NombreCompania.Trim(); } set { _NombreCompania = value; } }

        private string _Tipo;
        public string Tipo { get { return _Tipo.Trim(); } set { _Tipo = value; } }

        private string _Concepto;
        public string Concepto { get { return string.IsNullOrEmpty(_Concepto) ? string.Empty : _Concepto.Trim(); } set { _Concepto = value; } }
        //public string Concepto { get { return _Concepto.Trim(); } set { _Concepto = value; } }

        private string _Cuenta;
        public string Cuenta { get { return string.IsNullOrEmpty(_Cuenta) ? string.Empty : _Cuenta.Trim(); } set { _Cuenta = value; } }

        private string _Descripcion;
        public string Descripcion { get { return string.IsNullOrEmpty(_Descripcion) ? string.Empty : _Descripcion.Trim(); } set { _Descripcion = value; } }

        private string _Titulo;
        public string Titulo { get { return string.IsNullOrEmpty(_Titulo) ? string.Empty : _Titulo.Trim(); } set { _Titulo = value; } }

        private string _NombreArchivoXLS;
        public string NombreArchivoXLS { get { return _NombreArchivoXLS.Trim(); } set { _NombreArchivoXLS = value; } }

        private string _RFC;
        public string RFC { get { return string.IsNullOrEmpty(_RFC) ? string.Empty : _RFC.Trim(); } set { _RFC = value; } }
        public string NombreArchivoZIP { get; set; }

    }

}
