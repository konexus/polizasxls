﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using ClosedXML.Excel;
using System.Xml.Linq;
using System.Net;
using System.Net.Mail;
using Ionic.Zip;
namespace PolizasXLS
{
    public class PolizasController
    {
        public bool lfCreateBakFiles()
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(@ConfigurationManager.AppSettings["sRutaOrigen"], "_TDS");
                    zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                    zip.Save(string.Concat(@ConfigurationManager.AppSettings["sRutaRespaldo"], @"\", DateTime.Now.ToString("yyyy-MM-dd"), ".zip"));
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;

            }
        }
        public List<FileInfo> ConsultaPolizasXLS(string sRutaOrigen, ref DataTable dtPolizasAlternas, ref DataTable dtPolizasRegistradas, ref DataTable dtEmpresaDetalles, string sVersionProceso)
        {
            DirectoryInfo DI = new DirectoryInfo(sRutaOrigen);
            dtPolizasAlternas = ConsultaArchivosNominaAlterna(4).Copy();
            dtPolizasRegistradas = ConsultaArchivosNominaAlterna(2).Copy();
            dtEmpresaDetalles = ConsultaArchivosNominaAlterna(3).Copy();
            List<FileInfo> FIListado = DI.GetFiles("*.xls", SearchOption.AllDirectories).AsParallel().ToList();
            // FileInfo[] FIListado = DI.GetFiles();
            //List<FileInfo> FIListadoXLS = new List<FileInfo>();
            if (FIListado.Count() > 0)
            {
                //foreach (FileInfo item in FIListado)
                //{
                //    CreaControlRegistroArchivos(item.FullName, 0, sVersionProceso);
                //    if (Path.GetExtension(item.FullName.ToLower()) == ".xls")
                //        FIListadoXLS.Add(item);
                //}
                //return FIListadoXLS;
                return FIListado;
            }

            else
                return null;
        }
        private DataTable ConsultaArchivosNominaAlterna(int Opcion)
        {
            DataTable dt = new DataTable();
            SQLConection context = new SQLConection();
            context.Parametros.Clear();
            context.Parametros.Add(new SqlParameter("@Opcion", Opcion));
            dt = context.ExecuteProcedure("[sp_Poliza_Nominas_Configuracion]", true).Copy();
            return dt;
        }
        /// <summary>
        /// Convierte archivo xls a xlsx
        /// </summary>
        /// <param name="sNombreArchivo">Fuente</param>
        /// <returns>Respuesta si se logro hacer ho no</returns>
        public bool CreaArchivoXLStoXLSX(string sNombreArchivo)
        {
            try
            {
                BorraArchivoDirectorio(@sNombreArchivo + "x", 0);
                Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.Visible = false;
                Microsoft.Office.Interop.Excel.Workbook eWorkbook = excelApp.Workbooks.Open(@sNombreArchivo, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                eWorkbook.SaveAs(sNombreArchivo + "x", Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                eWorkbook.Close(false, Type.Missing, Type.Missing);
                return true;
            }
            catch
            {
                //Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool CreaProcesoxArchivo(FileInfo sNombreArchivo, DataTable dtPolizasAlternas, ref PolizaExcelDM op)
        {
            string sArchivoZIP = (Path.GetExtension(sNombreArchivo.FullName) == ".xls") ? sNombreArchivo.FullName.Replace(".xls", ".zip") : sNombreArchivo.FullName.Replace(".XLS", ".zip");
            XLWorkbook archivoxls = new XLWorkbook(string.Concat(sNombreArchivo.FullName, "x"));
            IXLWorksheet tab = archivoxls.Worksheet(1);

            try
            {
                int indiceUltimoRegistro = tab.LastRowUsed().RowNumber();
                string sDebito = string.Concat("SUM(H1:H", indiceUltimoRegistro, ")");
                string sCredito = string.Concat("SUM(I1:I", indiceUltimoRegistro, ")");
                string sRangoBusqueda = string.Concat("G1:G", indiceUltimoRegistro);
                bool banderatmp = false;
                List<string> NombresArchivosDentroZiptemp = new List<string>();
                op.Debito = decimal.Parse(tab.Evaluate(sDebito).ToString());
                op.Credito = decimal.Parse(tab.Evaluate(sCredito).ToString());
                if (File.Exists(@sArchivoZIP))
                {
                    banderatmp = true;
                    op.NombreArchivoZIP = @sArchivoZIP;
                }

                if (tab.Cell("A" + indiceUltimoRegistro).Value.ToString().Trim() != "0")
                {
                    op.Bandera = false;
                    op.Mesaje = "Póliza con formato incorrecto";
                    return op.Bandera;
                }
                if (op.Debito != op.Credito)
                {
                    op.Bandera = false;
                    op.Mesaje = "Las columnas Débito y Crédito no concilian.";
                }
                else
                {
                    op.Bandera = true;
                    op.Compania = int.Parse(tab.Cell("A2").Value.ToString().Trim());
                    op.NombreCompania = tab.Cell("B2").Value.ToString().Trim();
                    op.Titulo = tab.Cell("J2").Value.ToString().Trim();
                    //op.Tipo = tab.Cell("C2").Value.ToString();
                    //op.Fecha = DateTime.Parse(tab.Cell("D2").Value.ToString());
                    //op.Concepto = tab.Cell("E2").Value.ToString();
                    //op.Cuenta = tab.Cell("F2").Value.ToString();
                    //op.Descripcion = tab.Cell("G2").Value.ToString();
                    if (banderatmp)
                    {
                        decimal dtotalXML = 0;
                        //List<string> listaXMLTemp = new List<string>();
                        bool banderaTemp = false;
                        string sMensajeTemp = string.Empty;
                        string sRFC = string.Empty;
                        op.NetoFiscal = true;
                        // PASA A SEGUNDO PLANO EL PROCESO DE EXTRACIÖN DE ARCHIVOS XML DENTRO DEL ZIP 
                        //Task tarea = new Task(() => ConsultaArchivosDentroZip(@sArchivoZIP, ref dtotalXML, ref banderaTemp, ref sMensajeTemp, ref sRFC, ref NombresArchivosDentroZiptemp));
                        //tarea.Start();
                        if (!ConsultaArchivosDentroZip(@sArchivoZIP, ref dtotalXML, ref banderaTemp, ref sMensajeTemp, ref sRFC, ref NombresArchivosDentroZiptemp))
                        {
                            op.Bandera = false;
                            op.Mesaje = sMensajeTemp;
                            op.NetoFiscal = false;
                            sArchivoZIP = string.Empty;
                            return false;
                        }

                        var resultadorango = tab.Cells(sRangoBusqueda);
                        foreach (var item in resultadorango)
                        {
                            if (item.Value.ToString().Trim().ToUpper() == "NETOFISCAL")
                            {
                                op.NetoFiscalDebito = decimal.Parse(item.CellRight().Value.ToString());
                                op.NetoFiscalCredito = decimal.Parse(item.CellRight(2).Value.ToString());
                                op.Bandera = true;
                                break;
                            }
                            else
                            {
                                op.Bandera = false;
                                op.Mesaje = "* No se encontro la columna NETOFISCAL dentro la poliza xls " + Path.GetFileName(op.NombreArchivoXLS);
                            }
                        }
                        //tarea.Wait();
                        if ((dtotalXML == op.NetoFiscalDebito || dtotalXML == op.NetoFiscalCredito) && banderaTemp && op.Bandera)
                        {
                            op.RFC = sRFC;
                            op.Bandera = true;
                            op.NombresArchivosDentroZip = NombresArchivosDentroZiptemp;
                            op.Mesaje = string.Concat("Archivo OK ", Path.GetFileName(op.NombreArchivoXLS));
                        }
                        else
                        {
                            op.Bandera = false;
                            if (System.Text.RegularExpressions.Regex.IsMatch(op.Mesaje, "No se encontro la columna NETOFISCAL"))
                                op.Mesaje = string.Concat(op.Mesaje, " <br/>* ", "El total dentro del zip ", Path.GetFileName(op.NombreArchivoZIP), " es  ", dtotalXML);
                            else
                                op.Mesaje = string.Concat(string.IsNullOrEmpty(sMensajeTemp) ? string.Empty : sMensajeTemp, " NETOFISCAL Debito: ", op.NetoFiscalDebito, " y Credito:", op.NetoFiscalCredito, " <br/>  El total dentro del zip ", Path.GetFileName(op.NombreArchivoZIP), " es  ", dtotalXML, " es distinto");
                        }
                    }
                    else
                    {
                        op.Bandera = false;
                        op.Mesaje = "BADZIP";
                        op.NetoFiscal = false;
                        sArchivoZIP = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                op.Mesaje = string.Concat(" * El Archivo esta vacío, esta corrupto o tiene formato incorrecto.<br/> Descripción: ", ex.Message, " - ", ex.InnerException);
                op.Bandera = false;
            }
            return op.Bandera;
        }
        /// <summary>
        /// Consulta archivos dentro del zip de tipo XML y extrae total de cada uno de ellos 
        /// </summary>
        /// <param name="sRutaArchivoZIP">Nombre completo del archivo zip</param>
        /// <param name="dtotalXML">Suma total dentro de todos los archivos zip</param>
        /// <param name="listaArchivos"></param>
        /// <param name="bandera"></param>
        /// <param name="Mensaje"></param>
        private bool ConsultaArchivosDentroZip(string sRutaArchivoZIP, ref decimal dtotalXML, ref bool bandera, ref string Mensaje, ref string RFC, ref List<string> NombresArchivosDentroZiptemp)
        {
            string sPathArchivos = string.Concat(@Path.GetDirectoryName(sRutaArchivoZIP), @"\", Path.GetFileNameWithoutExtension(@sRutaArchivoZIP));
            //List<string> listaArchivos = new List<string>();
            decimal dtotalXMLTemp = 0;
            try
            {
                // VALIDA QUE SI EXISTE FOLDER LO BORRE PARA EVITAR CONFLICTO ENTRE LA MANIPULACION DE ARCHIVOS
                BorraArchivoDirectorio(sPathArchivos, 1);
                Directory.CreateDirectory(@sPathArchivos);

                // DESCOMPRIME SOLO ARCHIVOS XML DENTRO DEL ZIP
                using (ZipFile zip = ZipFile.Read(@sRutaArchivoZIP))
                //using (ZipFile zip = new ZipFile())
                {
                    // ZipFile.Read(sRutaArchivoZIP);
                    // VALIDA QUE HALLA ARCHIVOS DENTRO DEL ZIP
                    if (zip.Count() > 0)
                    {
                        foreach (ZipEntry item in zip)
                        {
                            // VALIDA QUE HALLA ARCHIVOS DENTRO DEL ZIP DE TIP XML
                            if (Path.GetExtension(item.FileName).ToLower() == ".xml")
                            {
                                string sArchivoFueraZip = string.Concat(@sPathArchivos, @"\", item.FileName);
                                NombresArchivosDentroZiptemp.Add(sArchivoFueraZip);
                                item.Extract(sPathArchivos, ExtractExistingFileAction.OverwriteSilently);
                                dtotalXMLTemp += ConsultaElementosXML(sArchivoFueraZip, ref RFC);
                                bandera = true;
                                Mensaje = "OK";
                            }
                        }
                    }
                    else
                    {
                        Mensaje = string.Concat(" * Archivo zip ", Path.GetFileName(@sRutaArchivoZIP), " vacío o corrupto");
                        bandera = false;
                    }
                }
                dtotalXML = dtotalXMLTemp;
                return true;
                // BorraArchivoDirectorio(@sPathArchivos, 1);
            }
            catch (Exception ex)
            {
                BorraArchivoDirectorio(sPathArchivos, 1);
                Mensaje = string.Concat(" * Archivo zip error ", Path.GetFileName(sRutaArchivoZIP), " ,", ex.Message, " ,", ex.InnerException);
                bandera = false;
                CreaRegistroError(Mensaje, Path.GetFileName(@sRutaArchivoZIP));
                return false;
            }
        }

        /// <summary>
        /// Consulta total dentro de cfdi:Comprobante dentro de el archivo XML , en caso de causar una Excepción 
        /// se pone el total a 0 para que no interfiera en el ciclo en caso de haber mas de una factura
        /// ;Borra el archivo XML que se esta manipulando
        /// </summary>
        /// <param name="pNombreArchivo">Nombre del XML</param>
        /// <returns>Regresa el total del archivo</returns>
        public decimal ConsultaElementosXML(string pNombreArchivo, ref string sRFC)
        {
            decimal total = 0;
            //Load xml
            XNamespace cfdi = @"http://www.sat.gob.mx/cfd/3";
            XNamespace tfd = @"http://www.sat.gob.mx/TimbreFiscalDigital";
            try
            {
                XDocument xdoc = XDocument.Load(@pNombreArchivo);
                var vc = xdoc.Element(cfdi + "Comprobante");
                var ve = xdoc.Element(cfdi + "Comprobante").Element(cfdi + "Emisor");
                total = (decimal)vc.Attribute("total");
                sRFC = (string)ve.Attribute("rfc");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                total = 0;
                sRFC = "XAXX010101000";
            }

            return total;

        }
        private void CreaEmailxPoliza(PolizaExcelDM poliza, int iTipo)
        {
            try
            {
                List<string> listaContactos = new List<string>();
                string sMsg = string.Empty;
                // Polizas sin detalles
                var EmailloginInfo = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPassword"]);
                var Emailmsg = new MailMessage();
                var EmailsmtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailSMTP"], int.Parse(ConfigurationManager.AppSettings["EmailPORT"]));
                Emailmsg.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"]);
                // Polizas con detalles
                if (iTipo == 0)
                {
                    Emailmsg.Subject = string.Concat("POLIZA OK ", Path.GetFileName(poliza.NombreArchivoXLS));
                    listaContactos = ConfigurationManager.AppSettings["EmailOK"].ToString().Split('|').ToList();
                    sMsg = string.Concat("Compañia: ", poliza.NombreCompania, "<br/>", "Poliza: ", Path.GetFileName(poliza.NombreArchivoXLS), "<br/>", "Detalle: ");
                }
                else
                {
                    Emailmsg.Subject = string.Concat("POLIZA ERROR ", Path.GetFileName(poliza.NombreArchivoXLS));
                    listaContactos = ConfigurationManager.AppSettings["EmailNO"].ToString().Split('|').ToList();
                    sMsg = string.Concat("Compañia: ", poliza.NombreCompania, "<br/>", "Poliza: ", Path.GetFileName(poliza.NombreArchivoXLS), "<br/>", "Detalle: ", poliza.Mesaje);
                }
                foreach (string contacto in listaContactos)
                {
                    if (!string.IsNullOrEmpty(contacto))
                        Emailmsg.To.Add(new MailAddress(contacto));
                }

                Emailmsg.IsBodyHtml = true;
                Emailmsg.Body = sMsg;
                EmailsmtpClient.EnableSsl = false;
                EmailsmtpClient.UseDefaultCredentials = false;
                EmailsmtpClient.Credentials = EmailloginInfo;
                EmailsmtpClient.Send(Emailmsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void CreaEmailGenerico(string sAsunto, string sMensaje, List<string> listaNombreArchivoAdjunto = null)
        {
            var Emailmsg = new MailMessage();
            try
            {
                List<string> listaContactos = new List<string>();
                string sMsg = string.Empty;
                // Polizas sin detalles  -- mira no tengo feria pero si tiempo y se que vale mas 
                var EmailloginInfo = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPassword"]);
                var EmailsmtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailSMTP"], int.Parse(ConfigurationManager.AppSettings["EmailPORT"]));
                Emailmsg.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"]);

                Emailmsg.Subject = sAsunto;
                listaContactos = ConfigurationManager.AppSettings["EmailNO"].ToString().Split('|').ToList();
                sMsg = sMensaje;

                foreach (string contacto in listaContactos)
                {
                    if (!string.IsNullOrEmpty(contacto))
                        Emailmsg.To.Add(new MailAddress(contacto));
                }

                if (listaNombreArchivoAdjunto != null)
                {
                    foreach (string item in listaNombreArchivoAdjunto)
                    {
                        Attachment aadjunto = new Attachment(@item);
                        Emailmsg.Attachments.Add(@aadjunto);
                    }
                }
                Emailmsg.IsBodyHtml = true;
                Emailmsg.Body = sMsg;
                EmailsmtpClient.EnableSsl = false;
                EmailsmtpClient.UseDefaultCredentials = false;
                EmailsmtpClient.Credentials = EmailloginInfo;
                EmailsmtpClient.Send(Emailmsg);
                Emailmsg.Dispose();
            }
            catch (Exception ex)
            {
                //    Console.WriteLine(ex.Message, "-", ex.InnerException);
                // Mensaje = string.Concat(" * Archivo zip error ", Path.GetFileName(sRutaArchivoZIP), " ,", ex.Message, " ,", ex.InnerException);
                //bandera = false;
                CreaRegistroError(sMensaje + "-" + sAsunto, ex.Message + "-- " + ex.InnerException);
            }
        }

        public List<PolizaExcelDM> CreaPreregistroPolizasXLS(ref List<PolizaExcelDM> listapolizasOK, DataTable dtPolizaRegistradas, DataTable dtEmpresaDetalle, DataTable dtPolizaOmite, int iOpcion)
        {
            List<PolizaExcelDM> listapolizasIncorrectas = new List<PolizaExcelDM>();
            bool bandera = false;
            foreach (PolizaExcelDM item in listapolizasOK)
            {
                string sMensajeTmp = string.Empty;
                DataRow[] drEmpresaExiste = dtEmpresaDetalle.Select("Numero_Alterno = " + item.Compania);
                if (drEmpresaExiste.Count() > 0)
                {

                    if (iOpcion == 0 && !string.IsNullOrEmpty(item.NombreArchivoZIP))
                        bandera = true;
                    // Buscar sin ZIP
                    else if (iOpcion == 1)
                    {
                        string[] sTemppatronbusquedad = Path.GetFileNameWithoutExtension(item.NombreArchivoXLS).Split('_');
                        foreach (string busca in sTemppatronbusquedad)
                        {
                            //string sTemp_query = string.Concat("Numero_Alterno = ", item.Compania, " AND DESCRIPCION  = '", busca, "'");
                            string sTemp_query = string.Concat("Numero_Alterno = ", item.Compania, " AND DESCRIPCION  LIKE '", busca, "%'");
                            DataRow[] drTemp = dtPolizaOmite.Select(sTemp_query);
                            if (drTemp.Count() > 0)
                            {
                                bandera = true;
                                item.RFC = drTemp[0]["RFC"].ToString().Trim();
                                break;
                            }

                        }
                    }

                    // GUARDAR SOLAMENTE SI ESTA ENTRE LAS CONDICIONES DE LA OTRA TABLA  "NO RECUERDO SU NOMBRE " EN CASO CONTRARIO DAR CUELLO
                    if (bandera)
                    {
                        if (string.IsNullOrEmpty(item.RFC))
                        {
                            item.Bandera = false;
                            item.Mesaje = (iOpcion == 0) ? string.Concat("El RFC de la poliza de la compañia ", item.Compania, " es distinto al registrado dentro del sistema RFC: '", drEmpresaExiste[0]["RFC"].ToString(), "' ") : string.Concat(" Verificar RFC   '", drEmpresaExiste[0]["RFC"].ToString(), "'  de la poliza alterna de la compañia ", item.Compania, " y/o <br />  no se encuentra parametrizada  póliza alterna'");
                            listapolizasIncorrectas.Add(item);
                            continue;
                        }
                        if (item.RFC.Trim() == drEmpresaExiste[0]["RFC"].ToString().Trim())
                        {
                            bool banderatmp = ConsultaInformacionXLS(@item.NombreArchivoXLS, int.Parse(drEmpresaExiste[0]["Numero"].ToString()), ref sMensajeTmp, item.NombresArchivosDentroZip);
                            if (banderatmp)
                            {
                                // PASAR EL ARCHIVO A SU FOLDER CORRESPONDIENTE
                                item.Bandera = true;
                                CopiaArchivoXMLOK(drEmpresaExiste[0]["Nombre_Corto"].ToString(), @item.Titulo.Trim(), @item.NombresArchivosDentroZip);
                                CreaRegistroArchivosProcesados(Path.GetFileNameWithoutExtension(item.NombreArchivoXLS), item.Compania);
                                //if (iOpcion == 1)
                                //    BorraArchivoDirectorio(@item.NombreArchivoXLS, 0);
                                //File.Delete(item.NombreArchivoXLS);
                            }
                            else
                            {
                                //listapolizasOKTemp.RemoveAt(indice);
                                item.Bandera = false;
                                item.Mesaje = sMensajeTmp;
                                listapolizasIncorrectas.Add(item);
                                continue;
                            }
                        }
                        else
                        {
                            item.Bandera = false;
                            item.Mesaje = string.Concat("La compañia ", item.Compania, "  es incorrecta verificicar la existencia de esta empresa");
                            listapolizasIncorrectas.Add(item);
                        }
                    }
                }
                else
                {
                    item.Bandera = false;
                    item.Mesaje = string.Concat("Verificar parametrización de la Empresa  Número: ", item.Compania, "   Nombre:", item.NombreCompania, "  RFC:", item.RFC, " .");
                    listapolizasIncorrectas.Add(item);
                }
            }

            return listapolizasIncorrectas;
        }

        public Boolean BorraArchivosFolder(string sDirectorio)
        {
            int intento = 0;
            IntentaNuevamente:
            try
            {
                DirectoryInfo folder = new DirectoryInfo(@sDirectorio);
                foreach (System.IO.DirectoryInfo subDirectory in folder.GetDirectories()) subDirectory.Delete(true);
                foreach (System.IO.FileInfo file in folder.GetFiles("*")) file.Delete();
                return true;
            }
            catch
            {
                //Console.WriteLine("Error borrar archivos carpeta" + ex.Message, ex.InnerException);
                intento++;
                if (intento == 3)
                    return false;
            }
            goto IntentaNuevamente;
        }
        /// <summary>
        /// Borra archivo o directorio siempre y cuando existe y con un maximo de 3 intentos
        /// </summary>
        /// <param name="sNombreArchivoDirectorio">Nombre del archivo o directorio</param>
        /// <param name="Opcion">0 Archivo , 1  Directorio</param>
        /// <returns>Bandera </returns>
        public bool BorraArchivoDirectorio(string sNombreArchivoDirectorio, int Opcion)
        {
            int intento = 0;
            IntentaNuevamente:
            try
            {
                if (Opcion == 0)
                {
                    if (File.Exists(@sNombreArchivoDirectorio))
                        File.Delete(@sNombreArchivoDirectorio);
                    return true;
                }
                if (Opcion == 1)
                {
                    if (Directory.Exists(@sNombreArchivoDirectorio))
                    {
                        BorraArchivosFolder(@sNombreArchivoDirectorio);
                        Directory.Delete(@sNombreArchivoDirectorio);
                    }
                    return true;

                }
                if (Opcion == 2)
                {
                    while (Opcion <= 4)
                    {
                        BorraArchivosFolder(@sNombreArchivoDirectorio);
                        Directory.CreateDirectory(@sNombreArchivoDirectorio);
                        Opcion++;
                    }

                    return true;

                }
            }
            catch
            {
                intento++;
                if (intento == 3)
                    return false;
            }
            goto IntentaNuevamente;
        }

        public void CreaDetalleEmailOK(string shorainicio, string shorafin, List<PolizaExcelDM> listapolizas)
        {
            if (listapolizas.Count > 0)
            {
                int iCantidadPolizas = listapolizas.Count;
                string sEmailCuerpo = string.Empty;
                string sHead = string.Empty;
                string sFooter = string.Empty;
                // HEAD
                sHead = string.Concat("<html><body><style>.espacio{padding:0 15px 0 15px;}</style><h3>Buen D&iacute;a</h3><p style='font-size:12px; font-family: Arial;'>Se han procesado correctamente ", iCantidadPolizas, " archivos el d&iacute;a ", DateTime.Now.ToShortDateString(), "  <br/>Inicio de an&aacute;lisis ", shorainicio, " <br/>Termino del an&aacute;lisis ", @shorafin, "<br/>Resumen:</p><table><tr ><th class='espacio'>Nombre de Archivo </th><th class='espacio'> Empresa </th><th class='espacio'> Total Debito </th><th class='espacio'> Total Cr&eacute;dito</th><th class='espacio'>NETOFISCAL</th></tr><tr><td colspan='5' style='border-bottom: 1px solid black !important;' /></tr><tr><td colspan='5' /></tr>");
                // FOOTER
                sFooter = string.Concat("<br/><table cellpadding='0' cellspacing='0' border='0' style='font-size:12px; font-family: Arial; line-height: 17px;' width='100%'><tbody><tr><td width='50%' style='background-color: #28487a; padding:10px;'><span style='color: #fff; font-size: 15px; font-weight: bold;'>INTEGRASOFTWARE</span><br><span style='color:#fff'>Soluci&oacute;n all-inclusive para empresas</span></td><td align='right' style='background-color:#f7f7f7; padding: 10px; font-size: 15px; font-weight: bold; color: #000'><img src='http://www.grupotds.com/imagen/lgoo_r1_c1.jpg' alt='img' height='70px'/> </td></tr><tr><td colspan='2' height='5' style='line-height: 5px; font-size: 5px;'>&nbsp;</td></tr><tr><td width='580' style='font-size:8px; font-family:Verdana; color:#959595; line-height: 10px; padding-bottom: 10px;' colspan='2'><p style='margin: 0px; padding-top: 10px;'> <span style='font-size:10px;'>Integra ERP Mexico DF &#8226 TODOS LOS DERECHOS RESERVADOS &#8226 (55) 5207 3799 &#8226 info@integrasoftware.com.mx &#8226</span><br><br/>Este men	saje est&aacute; dirigido exclusivamente a las personas que tienen las direcciones de correo electr&oacute;nico especificadas en los destinatarios dentro de su encabezado. Si por error usted ha recibido este mensaje, por ning&uacute;n motivo debe revelar su contenido, copiarlo, distribuirlo o utilizarlo. Le solicitamos por favor comunique del error a la direcci&oacute;n de correo electr&oacute;nico remitente y elimine dicho mensaje junto con cualquier documento adjunto que pudiera contener. Los derechos de privacidad y confidencialidad de la informaci&oacute;n en este mensaje no deben perderse por el hecho de haberse trasmitido err&oacute;neamente o por causas de interferencias en el funcionamiento de los sistemas de correo y canales de comunicaci&oacute;n. En consideraci&oacute;n a que los mensajes enviados de manera electr&oacute;nica pueden ser interceptados y manipulados, la empresa y las entidades que lo integran no se hacen responsables si los mensajes llegan con demora, incompletos, eliminados o con alg&uacute;n programa malicioso denominado como virus inform&aacute;tico. </p></td></tr></tbody></table></body></html>");
                int indice = 0;
                foreach (PolizaExcelDM item in listapolizas)
                {
                    sEmailCuerpo += string.Concat("<tr><td class='espacio'>", ++indice, "  ", Path.GetFileName(item.NombreArchivoXLS), "</td><td>", item.NombreCompania, "</td>",
                        "<td class='espacio' align='right'>", item.Debito, "</td> <td class='espacio' align='right'>", item.Credito, "</td><td class='espacio' align='right'>", ((item.NetoFiscal) ? (item.NetoFiscalDebito + item.NetoFiscalCredito).ToString() : "NA"), "</td></tr>");

                    //BorraArchivoDirectorio(item.NombreArchivoXLS, 0);
                    //File.Delete(item.NombreArchivoXLS);
                    //BorraArchivoDirectorio(item.NombreArchivoXLS, 0);

                }
                CreaEmailGenerico(string.Concat("Polizas Correctas ", DateTime.Now.ToShortDateString()), string.Concat(sHead, sEmailCuerpo, sFooter));
            }
        }
        public void CreaDetalleEmailError(string shorafin, List<PolizaExcelDM> listapolizas)
        {
            if (listapolizas.Count > 0)
            {
                string sEmailCuerpo = string.Empty;
                string sHead = string.Empty;
                string sFooter = string.Empty;

                foreach (PolizaExcelDM item in listapolizas)
                {
                    List<string> listatemp = new List<string>();
                    // HEAD AND BODY
                    sHead = string.Concat("<html><body><h4>Buen D&iacute;a</h4><p>P&oacute;liza. incorrecta (se adjunta archivo).</br>D&iacute;a: ", DateTime.Now.ToShortDateString(), "</br>Hora: ", shorafin, "</br>Archivo: ", Path.GetFileName(@item.NombreArchivoXLS), "</br>Detalle: ", item.Mesaje, "</br></br>");
                    // FOOTER
                    sFooter = string.Concat("<br/><table cellpadding='0' cellspacing='0' border='0' style='font-size:12px; font-family: Arial; line-height: 17px;' width='100%'><tbody><tr><td width='50%' style='background-color: #ff9444; padding:10px;'><span style='color: #fff; font-size: 15px; font-weight: bold;'>INTEGRASOFTWARE</span><br><span style='color:#fff'>Soluci&oacute;n all-inclusive para empresas</span></td><td align='right' style='background-color:#f7f7f7; padding: 10px; font-size: 15px; font-weight: bold; color: #000'><img src='http://www.grupotds.com/imagen/lgoo_r1_c1.jpg' alt='img' height='70px'/> </td></tr><tr><td colspan='2' height='5' style='line-height: 5px; font-size: 5px;'>&nbsp;</td></tr><tr><td width='580' style='font-size:8px; font-family:Verdana; color:#959595; line-height: 10px; padding-bottom: 10px;' colspan='2'><p style='margin: 0px; padding-top: 10px;'> <span style='font-size:10px;'>Integra ERP Mexico DF &#8226 TODOS LOS DERECHOS RESERVADOS &#8226 (55) 5207 3799 &#8226 info@integrasoftware.com.mx &#8226</span><br><br/>Este men	saje est&aacute; dirigido exclusivamente a las personas que tienen las direcciones de correo electr&oacute;nico especificadas en los destinatarios dentro de su encabezado. Si por error usted ha recibido este mensaje, por ning&uacute;n motivo debe revelar su contenido, copiarlo, distribuirlo o utilizarlo. Le solicitamos por favor comunique del error a la direcci&oacute;n de correo electr&oacute;nico remitente y elimine dicho mensaje junto con cualquier documento adjunto que pudiera contener. Los derechos de privacidad y confidencialidad de la informaci&oacute;n en este mensaje no deben perderse por el hecho de haberse trasmitido err&oacute;neamente o por causas de interferencias en el funcionamiento de los sistemas de correo y canales de comunicaci&oacute;n. En consideraci&oacute;n a que los mensajes enviados de manera electr&oacute;nica pueden ser interceptados y manipulados, la empresa y las entidades que lo integran no se hacen responsables si los mensajes llegan con demora, incompletos, eliminados o con alg&uacute;n programa malicioso denominado como virus inform&aacute;tico. </p></td></tr></tbody></table></body></html>");
                    listatemp.Add(item.NombreArchivoXLS);
                    if (!string.IsNullOrEmpty(item.NombreArchivoZIP))
                        listatemp.Add(item.NombreArchivoZIP);
                    //listatemp = listatemp.Count() > 2) ? listatemp.Take(2) : listatemp;
                    CreaEmailGenerico(string.Concat("Poliza Incorrecta: ", Path.GetFileName(item.NombreArchivoXLS)), string.Concat(sHead, sEmailCuerpo, sFooter), listatemp);
                    // BorraArchivoDirectorio(item.NombreArchivoXLS, 0);
                }
            }
        }
        public void CreaDetalleEmailyaRegistradas(string shorainicio, string shorafin, List<PolizaExcelDM> listapolizas)
        {
            if (listapolizas.Count > 0)
            {
                int iCantidadPolizas = listapolizas.Count();
                string sEmailCuerpo = string.Empty;
                string sHead = string.Empty;
                string sFooter = string.Empty;
                // HEAD
                sHead = string.Concat("<html><body><style>.espacio{padding:0 15px 0 15px;}</style><h3>Buen D&iacute;a</h3><p style='font-size:12px; font-family: Arial;'> Polizas ya registradas", iCantidadPolizas, " archivos el d&iacute;a ", DateTime.Now.ToShortDateString(), "  <br/>Inicio de an&aacute;lisis ", shorainicio, " <br/>Termino del an&aacute;lisis ", @shorafin, "<br/>Resumen:</p>"
                                     , "<table><tr><th class='espacio'>Nombre de Archivo </th></tr>",
                                             "<tr><td style='border-bottom: 1px solid black !important;'/></tr><tr><td /></tr>");
                // FOOTER
                sFooter = string.Concat("<br/><table cellpadding='0' cellspacing='0' border='0' style='font-size:12px; font-family: Arial; line-height: 17px;' width='100%'><tbody><tr><td width='50%' style='background-color: #28487a; padding:10px;'><span style='color: #fff; font-size: 15px; font-weight: bold;'>INTEGRASOFTWARE</span><br><span style='color:#fff'>Soluci&oacute;n all-inclusive para empresas</span></td><td align='right' style='background-color:#f7f7f7; padding: 10px; font-size: 15px; font-weight: bold; color: #000'><img src='http://www.grupotds.com/imagen/lgoo_r1_c1.jpg' alt='img' height='70px'/> </td></tr><tr><td colspan='2' height='5' style='line-height: 5px; font-size: 5px;'>&nbsp;</td></tr><tr><td width='580' style='font-size:8px; font-family:Verdana; color:#959595; line-height: 10px; padding-bottom: 10px;' colspan='2'><p style='margin: 0px; padding-top: 10px;'> <span style='font-size:10px;'>Integra ERP Mexico DF &#8226 TODOS LOS DERECHOS RESERVADOS &#8226 (55) 5207 3799 &#8226 info@integrasoftware.com.mx &#8226</span><br><br/>Este men	saje est&aacute; dirigido exclusivamente a las personas que tienen las direcciones de correo electr&oacute;nico especificadas en los destinatarios dentro de su encabezado. Si por error usted ha recibido este mensaje, por ning&uacute;n motivo debe revelar su contenido, copiarlo, distribuirlo o utilizarlo. Le solicitamos por favor comunique del error a la direcci&oacute;n de correo electr&oacute;nico remitente y elimine dicho mensaje junto con cualquier documento adjunto que pudiera contener. Los derechos de privacidad y confidencialidad de la informaci&oacute;n en este mensaje no deben perderse por el hecho de haberse trasmitido err&oacute;neamente o por causas de interferencias en el funcionamiento de los sistemas de correo y canales de comunicaci&oacute;n. En consideraci&oacute;n a que los mensajes enviados de manera electr&oacute;nica pueden ser interceptados y manipulados, la empresa y las entidades que lo integran no se hacen responsables si los mensajes llegan con demora, incompletos, eliminados o con alg&uacute;n programa malicioso denominado como virus inform&aacute;tico. </p></td></tr></tbody></table></body></html>");
                int indice = 0;
                foreach (PolizaExcelDM item in listapolizas)
                {
                    sEmailCuerpo += string.Concat("<tr><td class='espacio'>", ++indice, "  ", Path.GetFileName(item.NombreArchivoXLS), "</td></tr>");
                    //BorraArchivoDirectorio(item.NombreArchivoXLS, 0);
                }
                CreaEmailGenerico(string.Concat("Polizas ya estan registradas ", DateTime.Now.ToShortDateString()), string.Concat(sHead, sEmailCuerpo, sFooter));
            }
        }

        public void CreaDetalleEmailResumen(int iPolizasOK, int iPolizasIncorrectas, int iPolizasAlternasOK, int iPolizasAlternasIncorrectas, int iPolizasRegistradas)
        {
            int TotalPolizas = iPolizasOK + iPolizasIncorrectas + iPolizasAlternasOK + iPolizasAlternasIncorrectas + iPolizasRegistradas;
            //if (listapolizas.Count > 0)
            //{
            string sEmailCuerpo = string.Empty;
            string sHead = string.Empty;
            string sFooter = string.Empty;
            // HEAD1
            sHead = string.Concat("<html><body><h2>Buen D&iacute;a</h2><h2>Resumen de Operaci&oacute;n</h2>");
            sEmailCuerpo = string.Concat("<p>P&oacute;lizas correctas :", iPolizasOK, "<br/>", "P&oacute;lizas incorrectas :", iPolizasIncorrectas, "<br/>", "P&oacute;lizas correctas alternas :", iPolizasAlternasOK, "<br/>", "P&oacute;lizas incorrectas alternas :", iPolizasAlternasIncorrectas, "<br/>", "P&oacute;lizas ya registradas :", iPolizasRegistradas, "<br/>", "Total de P&oacute;lizas analizadas:", TotalPolizas, "<p/>");
            // FOOTER
            sFooter = string.Concat("<br/><table cellpadding='0' cellspacing='0' border='0' style='font-size:12px; font-family: Arial; line-height: 17px;' width='100%'><tbody><tr><td width='50%' style='background-color: #b5d600; padding:10px;'><span style='color: #fff; font-size: 15px; font-weight: bold;'>INTEGRASOFTWARE</span><br><span style='color:#fff'>Soluci&oacute;n all-inclusive para empresas</span></td><td align='right' style='background-color:#f7f7f7; padding: 10px; font-size: 15px; font-weight: bold; color: #000'><img src='http://www.grupotds.com/imagen/lgoo_r1_c1.jpg' alt='img' height='70px'/> </td></tr><tr><td colspan='2' height='5' style='line-height: 5px; font-size: 5px;'>&nbsp;</td></tr><tr><td width='580' style='font-size:8px; font-family:Verdana; color:#959595; line-height: 10px; padding-bottom: 10px;' colspan='2'><p style='margin: 0px; padding-top: 10px;'> <span style='font-size:10px;'>Integra ERP Mexico DF &#8226 TODOS LOS DERECHOS RESERVADOS &#8226 (55) 5207 3799 &#8226 info@integrasoftware.com.mx &#8226</span><br><br/>Este men	saje est&aacute; dirigido exclusivamente a las personas que tienen las direcciones de correo electr&oacute;nico especificadas en los destinatarios dentro de su encabezado. Si por error usted ha recibido este mensaje, por ning&uacute;n motivo debe revelar su contenido, copiarlo, distribuirlo o utilizarlo. Le solicitamos por favor comunique del error a la direcci&oacute;n de correo electr&oacute;nico remitente y elimine dicho mensaje junto con cualquier documento adjunto que pudiera contener. Los derechos de privacidad y confidencialidad de la informaci&oacute;n en este mensaje no deben perderse por el hecho de haberse trasmitido err&oacute;neamente o por causas de interferencias en el funcionamiento de los sistemas de correo y canales de comunicaci&oacute;n. En consideraci&oacute;n a que los mensajes enviados de manera electr&oacute;nica pueden ser interceptados y manipulados, la empresa y las entidades que lo integran no se hacen responsables si los mensajes llegan con demora, incompletos, eliminados o con alg&uacute;n programa malicioso denominado como virus inform&aacute;tico. </p></td></tr></tbody></table></body></html>");

            // int indice = 0;
            //foreach (PolizaExcelDM item in listapolizas)
            //{
            //    sEmailCuerpo += string.Concat("<tr><td class='espacio'>", ++indice, "  ", Path.GetFileName(item.NombreArchivoXLS), "</td></tr>");
            //    //BorraArchivoDirectorio(item.NombreArchivoXLS, 0);
            //}
            CreaEmailGenerico(string.Concat("Proceso Completo (Resumen)", DateTime.Now.ToShortDateString()), string.Concat(sHead, sEmailCuerpo, sFooter));
            //}
        }

        private bool ConsultaInformacionXLS(string sNombreArchivoXLS, int EmpresaInterna, ref string sMensaje, List<string> listaArchivosXMLenZIP)
        {
            XLWorkbook archivoxls = new XLWorkbook(string.Concat(sNombreArchivoXLS, "x"));
            IXLWorksheet tab = archivoxls.Worksheet(1);
            int indiceUltimoRegistro = tab.LastRowUsed().RowNumber();
            string sRangoBusqueda = string.Concat("A2:A", indiceUltimoRegistro);
            var resultadorango = tab.Cells(sRangoBusqueda);

            foreach (var item in resultadorango)
            {
                PolizaExcelDM op = new PolizaExcelDM();
                try
                {
                    op.Compania = int.Parse(item.Value.ToString()) == 0 ? 0 : EmpresaInterna;
                    op.NombreCompania = string.IsNullOrEmpty(item.CellRight().Value.ToString().Trim()) ? tab.Cell(string.Concat("B", indiceUltimoRegistro - 1)).Value.ToString().Trim() : item.CellRight().Value.ToString().Trim();
                    op.Tipo = string.IsNullOrEmpty(item.CellRight(2).Value.ToString().Trim()) ? tab.Cell(string.Concat("C", indiceUltimoRegistro - 1)).Value.ToString().Trim() : item.CellRight(2).Value.ToString().Trim();
                    op.Fecha = string.IsNullOrEmpty(item.CellRight(3).Value.ToString().Trim().Trim()) ? DateTime.Parse(tab.Cell(string.Concat("D", indiceUltimoRegistro - 1)).Value.ToString().Trim()) : DateTime.Parse(item.CellRight(3).Value.ToString().Trim());
                    op.Concepto = string.IsNullOrEmpty(item.CellRight(4).Value.ToString().Trim()) ? tab.Cell(string.Concat("E", indiceUltimoRegistro - 1)).Value.ToString().Trim() : item.CellRight(4).Value.ToString().Trim();
                    op.Cuenta = string.IsNullOrEmpty(item.CellRight(5).Value.ToString().Trim()) ? tab.Cell(string.Concat("F", indiceUltimoRegistro - 1)).Value.ToString().Trim() : item.CellRight(5).Value.ToString().Trim();
                    op.Descripcion = string.IsNullOrEmpty(item.CellRight(6).Value.ToString().Trim()) ? tab.Cell(string.Concat("G", indiceUltimoRegistro - 1)).Value.ToString().Trim() : item.CellRight(6).Value.ToString().Trim();
                    op.Debito = string.IsNullOrEmpty(item.CellRight(7).Value.ToString().Trim()) ? 0 : decimal.Parse(item.CellRight(7).Value.ToString().Trim());
                    op.Credito = string.IsNullOrEmpty(item.CellRight(8).Value.ToString().Trim()) ? 0 : decimal.Parse(item.CellRight(8).Value.ToString().Trim());
                    op.Titulo = string.IsNullOrEmpty(item.CellRight(9).Value.ToString().Trim()) ? tab.Cell(string.Concat("J", indiceUltimoRegistro - 1)).Value.ToString().Trim() : item.CellRight(9).Value.ToString().Trim();
                    op.NombreArchivoXLS = @sNombreArchivoXLS;
                    op.NombresArchivosDentroZip = listaArchivosXMLenZIP;
                    CreaeIncorporaPoliza(op);
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("ConsultaInformacionXLSr" + ex.Message + ex.InnerException);
                    sMensaje = string.Concat("No se completo el registro de poliza XLS; <br/> Compania:", op.Compania, "<br/>  Nombre:", op.NombreCompania, "<br/> Tipo:", op.Tipo, "<br/> Fecha:", op.Fecha, "<br/>  Concepto:", op.Concepto, "<br/> Cuenta:", op.Cuenta, "<br/>  Descripcion:", op.Descripcion, "<br/>  Debito:", op.Debito, "<br/>  Credito:", op.Credito, "<br/> Titulo:", op.Titulo, "<br/>", ex.Message, "; ", ex.InnerException);
                    //  BorraArchivoDirectorio(string.Concat(sNombreArchivoXLS, "x"), 0);
                    File.Delete(string.Concat(@sNombreArchivoXLS, "x"));
                    return false;
                }
            }
            File.Delete(string.Concat(@sNombreArchivoXLS, "x"));

            //BorraArchivoDirectorio(string.Concat(sNombreArchivoXLS, "x"), 0);
            return true;
        }

        public void CreaeIncorporaPoliza(PolizaExcelDM op)
        {

            if (op.Compania.ToString().Length > 4)
                throw new Exception("* Verificar número de compañía ");

            op.NombreCompania = op.NombreCompania.Trim().Length > 250 ? op.NombreCompania.Trim().Substring(0, 250) : op.NombreCompania.Trim();
            op.Tipo = op.Tipo.Trim().Length > 100 ? op.Tipo.Trim().Substring(0, 100) : op.Tipo.Trim();
            op.Concepto = op.Concepto.Trim().Length > 250 ? op.Concepto.Trim().Substring(0, 250) : op.Concepto.Trim();
            op.Cuenta = op.Cuenta.Trim().Length > 30 ? op.Cuenta.Trim().Substring(0, 30) : op.Cuenta.Trim();
            op.Titulo = op.Titulo.Trim().Length > 80 ? op.Titulo.Trim().Substring(0, 80) : op.Titulo.Trim();

            DataTable dt = new DataTable();
            SQLConection context = new SQLConection();
            context.Parametros.Clear();
            context.Parametros.Add(new SqlParameter("@Compania", op.Compania));
            context.Parametros.Add(new SqlParameter("@Nombre_Compania", op.NombreCompania.Trim()));
            context.Parametros.Add(new SqlParameter("@Tipo", op.Tipo.Trim()));
            context.Parametros.Add(new SqlParameter("@Fecha", op.Fecha));
            context.Parametros.Add(new SqlParameter("@Concepto", op.Concepto.Trim()));
            context.Parametros.Add(new SqlParameter("@Cuenta", op.Cuenta.Trim()));
            context.Parametros.Add(new SqlParameter("@Deber", op.Debito));
            context.Parametros.Add(new SqlParameter("@Haber", op.Credito));
            context.Parametros.Add(new SqlParameter("@Titulo", op.Titulo.Trim()));
            context.Parametros.Add(new SqlParameter("@Proyecto", null));
            context.Parametros.Add(new SqlParameter("@Cliente", null));
            context.Parametros.Add(new SqlParameter("@Folio_Fiscal", (op.NombresArchivosDentroZip == null) ? "" : "La carpeta contiene " + op.NombresArchivosDentroZip.Count + " archivos XML"));
            dt = context.ExecuteProcedure("[sp_Nomina_Contabilidad_XLS]", true).Copy();
        }

        private bool CopiaArchivoXMLOK(string sNombreCorto, string sTitulo, List<string> listaArchivos)
        {
            sTitulo = sTitulo.Replace("\n", "");
            string sPath = Path.Combine(@ConfigurationManager.AppSettings["CFDI_NOMINA"].ToString(), sNombreCorto, sTitulo);
            try
            {
                if (!Directory.Exists(@sPath))
                    Directory.CreateDirectory(sPath);

                foreach (string item in listaArchivos)
                    File.Copy(@item, string.Concat(@sPath, @"\", Path.GetFileName(item)), true);

                return true;
            }
            catch
            {

                return false;
            }
            finally
            {
                if (listaArchivos != null)
                    BorraArchivoDirectorio(Path.GetDirectoryName(listaArchivos[0]), 1);
            }
        }

        public bool CopiaArchivoTemp(string sNombreArchivo, int iOpcion)
        {
            string sPathCFDI_NOMINA_TMP = Path.Combine(@ConfigurationManager.AppSettings["CFDI_NOMINA_TMP"].ToString());
            string sRutaOrigen = Path.Combine(@ConfigurationManager.AppSettings["sRutaOrigen"].ToString());
            try
            {
                switch (iOpcion)
                {
                    case 0:
                        if (!Directory.Exists(@sPathCFDI_NOMINA_TMP))
                            Directory.CreateDirectory(@sPathCFDI_NOMINA_TMP);
                        else
                            File.Copy(sNombreArchivo, string.Concat(@sPathCFDI_NOMINA_TMP, @"\", Path.GetFileName(sNombreArchivo)), true);
                        break;

                    case 1:
                        DirectoryInfo DI = new DirectoryInfo(sPathCFDI_NOMINA_TMP);
                        FileInfo[] FIListadoXLS = DI.GetFiles("*.xls", SearchOption.TopDirectoryOnly).AsQueryable().ToArray();
                        if (FIListadoXLS.Count() > 0)
                        {
                            if (!Directory.Exists(@sRutaOrigen))
                                Directory.CreateDirectory(@sRutaOrigen);

                            foreach (var item in FIListadoXLS)
                                File.Copy(@item.FullName, string.Concat(sRutaOrigen, @"\", Path.GetFileName(item.FullName)), true);
                        }
                        BorraArchivosFolder(sPathCFDI_NOMINA_TMP);
                        break;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void CreaRegistroError(string sMensaje, string sNombreArchivo)
        {
            SQLConection context = new SQLConection();
            DataTable dt = new DataTable();
            context.Parametros.Clear();
            context.Parametros.Add(new SqlParameter("@UUID", "POLIZAS_XLS"));
            context.Parametros.Add(new SqlParameter("@RUTA", sMensaje));
            context.Parametros.Add(new SqlParameter("@PARAMETROS", sNombreArchivo));
            dt = context.ExecuteProcedure("[INTEGRA_KONEXUS_FAC].[dbo].[sp_LOG_FacturaRegistroError]", true).Copy();
            Console.Write("");
        }

        public void CreaRegistroArchivosProcesados(string sArchivoProcesado, int iCompania)
        {
            try
            {
                SQLConection context = new SQLConection();
                context.Parametros.Clear();
                context.Parametros.Add(new SqlParameter("@ArchivoProcesado", sArchivoProcesado));
                context.Parametros.Add(new SqlParameter("@compania", iCompania));
                context.ExecuteProcedure("[sp_Poliza_Nominas_Crea_RegistroArchivosProcesados]", true).Copy();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }


        public void CreaControlRegistroArchivos(string sNombreArchivo, int iOpcion, string sVersionProceso)
        {
            try
            {
                SQLConection context = new SQLConection();
                context.Parametros.Clear();
                context.Parametros.Add(new SqlParameter("@RUTA", @sNombreArchivo));
                context.Parametros.Add(new SqlParameter("@Opcion", iOpcion));
                context.Parametros.Add(new SqlParameter("@VersionProceso", sVersionProceso));
                context.ExecuteProcedure("[INTEGRA_KONEXUS_FAC].[dbo].[sp_LOG_ArchivosEntradaPolizas]", true).Copy();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error: " + ex.Message);
            }
        }
        public void lfSearchXLSX(string spath)
        {
            if (Directory.Exists(@spath))
            {
                DirectoryInfo DI = new DirectoryInfo(@spath);
                List<FileInfo> FIListado = DI.GetFiles("*.xlsx", SearchOption.AllDirectories).AsParallel().ToList();

                foreach (FileInfo item in FIListado)
                {
                    item.Delete();
                }
            }
        }

        public void lfDropBoxShareconfig(int ioption)
        {
            string spathorign = ConfigurationManager.AppSettings["sRutaOrigen"].ToString();
            string spathdestination = ConfigurationManager.AppSettings["sRutaOrigen"].ToString() + "_Z";


            if (!Directory.Exists(@spathorign))
                throw new Exception("Error no existe recurso principal");

            if (Directory.Exists(@spathdestination) && ioption == 0)
                Directory.Delete(@spathdestination, true);

            System.Threading.Thread.Sleep(3000);

            if (!Directory.Exists(@spathdestination) && ioption == 0)
                Directory.CreateDirectory(@spathdestination);

            // CREA RESPALDO 
            // TODO
            // 1 Validar que existan carpeta origen
            // 2 Validar que existan carpeta destino
            // 3 validar que exista el archivo  
            // 4 pasarr archivo a carpeta destino
            if (ioption == 0)
            {
                if (File.Exists(spathorign + @"\.dropbox"))
                {
                    File.Copy(spathorign + @"\.dropbox", spathdestination + @"\.dropbox", true);
                }
            }

            if (ioption == 1)
            {
                if (File.Exists(spathdestination + @"\.dropbox"))
                    File.Copy(spathdestination + @"\.dropbox", spathorign + @"\.dropbox", true);

                Directory.Delete(@spathdestination, true);
            }
        }
        public void CreaEmailPruebas(string sAsunto, string sMensaje, List<string> listaContactos)
        {
            var Emailmsg = new MailMessage();

            //List<string> listaContactos = new List<string>();
            string sMsg = string.Empty;
            // Polizas sin detalles  -- mira no tengo feria pero si tiempo y se que vale mas 
            //var EmailloginInfo = new NetworkCredential("stban@openmailbox.org", "JQuery0170");
            var EmailloginInfo = new NetworkCredential("nominatds@integrasoftware.com.mx", "nomina");
            var EmailsmtpClient = new SmtpClient("smtp.alestraune.net.mx", int.Parse("587"));
            Emailmsg.From = new MailAddress("nominatds@integrasoftware.com.mx");

            Emailmsg.Subject = sAsunto;
            //listaContactos = ConfigurationManager.AppSettings["EmailNO"].ToString().Split('|').ToList();
            sMsg = sMensaje;

            foreach (string contacto in listaContactos)
            {
                if (!string.IsNullOrEmpty(contacto))
                    Emailmsg.To.Add(new MailAddress(contacto));
            }

            //if (listaNombreArchivoAdjunto != null)
            //{
            //    foreach (string item in listaNombreArchivoAdjunto)
            //    {
            //        Attachment aadjunto = new Attachment(@item);
            //        Emailmsg.Attachments.Add(@aadjunto);
            //    }
            //}
            Emailmsg.IsBodyHtml = true;
            Emailmsg.Body = sMsg;
            EmailsmtpClient.EnableSsl = false;
            EmailsmtpClient.UseDefaultCredentials = false;
            EmailsmtpClient.Credentials = EmailloginInfo;
            EmailsmtpClient.Send(Emailmsg);
            Emailmsg.Dispose();

        }
    }


}
