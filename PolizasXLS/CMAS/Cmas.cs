﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Net.Mail;
using System.Net;
using System.Linq;
using System.IO;
using Ionic.Zip;

namespace PolizasXLS.CMAS
{
    class Cmas : ICmas
    {
        private List<CFDIModels> _list = new List<CFDIModels>();
        public bool getDesynchronized()
        {
            SQLConection context = new SQLConection();
            DataTable dt = new DataTable();
            try
            {
                context.Parametros.Clear();
                context.Parametros.Add(new SqlParameter("@XMLBody", ""));
                context.Parametros.Add(new SqlParameter("@UUID", ""));
                context.Parametros.Add(new SqlParameter("@STATUS", ""));
                context.Parametros.Add(new SqlParameter("@RFC", ""));
                context.Parametros.Add(new SqlParameter("@PATH", ""));
                context.Parametros.Add(new SqlParameter("@OPTIONALS", ""));
                context.Parametros.Add(new SqlParameter("@INCIDENCEDETAILS", ""));
                context.Parametros.Add(new SqlParameter("@CHOICE", 3));
                context.Parametros.Add(new SqlParameter("@ID", "1"));
                dt = context.ExecuteProcedure("[INTEGRA_KONEXUS_FAC].[dbo].[sp_CXC_Log]", true).Copy();

                foreach (DataRow item in dt.Rows)
                {
                    CFDIModels objcfdi = new CFDIModels();
                    objcfdi.ID = Convert.ToInt32(item["ID"].ToString());
                    objcfdi.sXML = item["FILEXML"].ToString();
                    objcfdi.sUUID = item["UUID"].ToString();
                    objcfdi.sDETAILS = item["DETAILS"].ToString();
                    objcfdi.sREQUEST = Convert.ToDateTime(item["REQUEST"].ToString());
                    objcfdi.sRESPONSABLE = item["RESPONSABLE"].ToString();
                    _list.Add(objcfdi);
                }
                return lfSendDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private bool lfSendDetails()
        {
            StringBuilder sb = new StringBuilder();
            string sZipTmp = string.Empty;
            List<string> listFiles = new List<string>();
            try
            {
                for (int i = 0; i < _list.Count; i++)
                {
                    XmlDocument xDocCFDI = new XmlDocument();
                    xDocCFDI.LoadXml(_list[i].sXML);
                    _list[i].sPath = @ConfigurationManager.AppSettings["sPathCmasCFDFI"] + i + "___" + _list[i].ID + ".xml";
                    listFiles.Add(@_list[i].sPath);
                    xDocCFDI.Save(_list[i].sPath);
                    sb.Append(Path.GetFileName(@_list[i].sPath)).Append("</br>").Append(_list[i].sREQUEST).Append("</br>").Append(_list[i].sRESPONSABLE).Append("</br>").Append(_list[i].sDETAILS).Append(" ========================================================================== </br></br>");
                }

                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(@ConfigurationManager.AppSettings["sPathCmasCFDFI"], "compress_cfdi");
                    zip.Comment = "Zip was created at " + System.DateTime.Now.ToString("G");
                    sZipTmp = (string.Concat(@ConfigurationManager.AppSettings["sPathCmasCFDFI"], @"\", DateTime.Now.ToString("yyyy-MM-dd"), ".zip"));
                    zip.Save(sZipTmp);
                }

                foreach (string item in listFiles)
                    File.Delete(@item);

                listFiles.Clear();
                listFiles.Add(sZipTmp);
                lfSendEmail(_list.Count + " CFDI de CMAS No se coinciliaron con INTEGRA ERP", sb.ToString(), listFiles);
                File.Delete(sZipTmp);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private bool lfSendEmail(string sSubject, string sMessage, List<string> sFile)
        {
            var Emailmsg = new MailMessage();
            try
            {
                List<string> listaContactos = new List<string>();
                string sMsg = string.Empty;

                var EmailloginInfo = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPassword"]);
                var EmailsmtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailSMTP"], int.Parse(ConfigurationManager.AppSettings["EmailPORT"]));
                Emailmsg.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"]);

                Emailmsg.Subject = sSubject;
                listaContactos = ConfigurationManager.AppSettings["EmailCMAS"].ToString().Split('|').ToList();
                sMsg = sMessage;

                foreach (string contacto in listaContactos)
                {
                    if (!string.IsNullOrEmpty(contacto))
                        Emailmsg.To.Add(new MailAddress(contacto));
                }

                //if (!string.IsNullOrEmpty(sFile))
                //{
                foreach (var item in sFile)
                {
                    Attachment aadjunto = new Attachment(item);
                    Emailmsg.Attachments.Add(@aadjunto);
                }

                //}
                Emailmsg.IsBodyHtml = true;
                Emailmsg.Body = sMsg;
                EmailsmtpClient.EnableSsl = false;
                EmailsmtpClient.UseDefaultCredentials = false;
                EmailsmtpClient.Credentials = EmailloginInfo;
                EmailsmtpClient.Send(Emailmsg);
                Emailmsg.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "-", ex.InnerException);
                // Mensaje = string.Concat(" * Archivo zip error ", Path.GetFileName(sRutaArchivoZIP), " ,", ex.Message, " ,", ex.InnerException);
                //bandera = false;
                return false;
            }
        }
    }
}
