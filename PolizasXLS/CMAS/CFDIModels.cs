﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolizasXLS.CMAS
{
    public class CFDIModels
    {
        public string sXML { get; set; } = string.Empty;

        public string sUUID { get; set; } = string.Empty;
        public string sDETAILS { get; set; } = string.Empty;
        public DateTime sREQUEST { get; set; } = DateTime.MinValue;
        public string sRESPONSABLE { get; set; } = string.Empty;
        public string sPath { get; set; } = string.Empty;
        public int ID { get; set; } = 0;
    }
}
