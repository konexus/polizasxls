﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Globalization;
using PolizasXLS.CMAS;
namespace PolizasXLS
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                if (DateTime.Now.Hour >= int.Parse(ConfigurationManager.AppSettings["sHourLimitStart"].ToString()) && DateTime.Now.Hour <= int.Parse(ConfigurationManager.AppSettings["sHourLimitEnd"].ToString()))
                {
                    try
                    {
                        lfMainCMAS();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error " + ex.Message);
                    }
                    try
                    {
                        lfMain();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error " + ex.Message);
                    }


                }
                Console.WriteLine("Proceso completo");
                Console.WriteLine("The hour is " + DateTime.Now.Hour.ToString() + " - the time is " + DateTime.Now);
                Thread.Sleep(Convert.ToInt32(TimeSpan.FromMinutes(50).TotalMilliseconds));
            }
        }

        /// <summary>
        /// Proceso que se ejecuta cada 2 horas
        /// </summary>
        static bool lfMain()
        {
            //var str = dateTime.ToString(@"yyyy/MM/dd hh:mm:ss tt", new CultureInfo("en-US"));
            Console.Clear();
            string sHoraProceso = string.Empty;
            DateTime dtIniciaProceso = DateTime.Now;
            string sTiempoInicio = DateTime.Now.ToShortTimeString();
            string sRutaOrigen = ConfigurationManager.AppSettings["sRutaOrigen"].ToString();
            string sRutaOrigenTMP = ConfigurationManager.AppSettings["CFDI_NOMINA_TMP"].ToString();
            // FileInfo fl = new FileInfo(ConfigurationManager.AppSettings["sRutaOrigen"].ToString())

            DataTable dtPolizaExcluye = new DataTable();
            DataTable dtPolizaRegistradas = new DataTable();
            DataTable dtEmpresaDetalles = new DataTable();
            PolizasController control = new PolizasController();
            List<PolizaExcelDM> listapoliza = new List<PolizaExcelDM>();
            List<PolizaExcelDM> listapolizaRegistradas = new List<PolizaExcelDM>();
            //List<PolizaExcelDM> listapolizassinzip = new List<PolizaExcelDM>();
            //string sHoraProceso = string.Format("{0:hh:mm:ss}", DateTime.Now);
            sHoraProceso = DateTime.Now.ToString("hh:mm:ss tt", new CultureInfo("es-MX"));
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("================================================");
            Console.WriteLine(" Here we go  ¡R o b o c o p!");
            Console.WriteLine("================================================");
            Console.ResetColor();
            Console.WriteLine(sHoraProceso);
            //if ((DateTime.Now >= DateTime.Parse("01:10:00 p. m.", new CultureInfo("es-MX"))) && (DateTime.Now >= DateTime.Parse("02:00:00 p. m.", new CultureInfo("es-MX"))))
            //{
            Console.WriteLine("Creando respaldo...");
            Stopwatch cronometro = new Stopwatch();
            cronometro.Start();
            // CREA RESPALDO 
            new PolizasController().lfDropBoxShareconfig(0);
            new PolizasController().lfSearchXLSX(sRutaOrigen);

            if (new PolizasController().lfCreateBakFiles())
                Console.WriteLine("OK Se creo respaldo correctamente");
            else
                Console.WriteLine("Error Al crear el respaldo");



            // 1 LISTA ARCHIVOS XLS , XLSX Y BUSCA XLS YA REGISTRADOS
            Console.WriteLine("BUSCANDO ARCHIVOS XLS ");
            string sVersionProceso = "V" + new Random().Next(0, 1000000).ToString();
            List<FileInfo> aArchivos = control.ConsultaPolizasXLS(sRutaOrigen, ref dtPolizaExcluye, ref dtPolizaRegistradas, ref dtEmpresaDetalles, sVersionProceso);

            // 2 RECORRE LOS ARCHIVOS ENCONTRADOS
            if (aArchivos != null)
            {
                int iNumeroArchivoTope = aArchivos.Count();
                int iNumeroIndice = 1;
                // Console.Clear();
                Console.WriteLine(" 1 SE ANALIZARAN " + aArchivos.Count());

                foreach (FileInfo item in aArchivos)
                {
                    // 3 VERIFICA QUE NO HALLAN SIDO YA REGISTRADOS LOS ARCHIVOS
                    string strQueryTemp = string.Concat("ArchivoProcesado = '", Path.GetFileNameWithoutExtension(item.FullName.ToUpper()), "'");
                    DataRow[] drPolizasRegistradas = dtPolizaRegistradas.Select(strQueryTemp);
                    PolizaExcelDM op = new PolizaExcelDM();
                    if (drPolizasRegistradas.Count() > 0)
                    {

                        op.NombreArchivoXLS = item.FullName;
                        op.Bandera = false;
                        op.Mesaje = "ARCHIVOREGISTRADO";
                        //listapolizaRegistradas.Add(opr);
                        //control.CreaControlRegistroArchivos(item.FullName, 1);
                        listapoliza.Add(op);
                        continue;
                    }

                    //PolizaExcelDM op = new PolizaExcelDM();
                    op.NombreArchivoXLS = item.FullName;
                    op.Bandera = false;

                    // 4 SI CREA ARCHIVO XLSX PARA MANIPULAR ARCHIVO  
                    if (control.CreaArchivoXLStoXLSX(item.FullName))
                    {
                        // 5 BUSCA CONCILIACION ENTRE CARGO Y ABONO , DESCOMPRIME ZIP Y ANALIZA CADA XML (EN CASO DE QUE EXISTA) REGRESA ESTATUS
                        op.Bandera = control.CreaProcesoxArchivo(item, dtPolizaExcluye, ref op);
                        //control.CreaControlRegistroArchivos(item.FullName, 1);
                        Console.Clear();
                        Console.WriteLine(string.Concat(iNumeroIndice++, " Archivo de ", iNumeroArchivoTope, " Procesando archivo ", item.Name));

                        if (op.Bandera == false)
                        {
                            if (op.Mesaje == "BADZIP")
                                control.CopiaArchivoTemp(@item.FullName, 0);
                        }
                    }
                    //else
                    //    control.CreaControlRegistroArchivos(item.FullName, 1);

                    //control.BorraArchivoDirectorio(@item.FullName + "x", 0);
                    listapoliza.Add(op);
                    control.CreaControlRegistroArchivos(item.FullName, 1, sVersionProceso);

                }
                Console.Clear();
                Console.WriteLine(" Filtro de polizas ");
                List<PolizaExcelDM> PolizasOK = listapoliza.Where(apok => apok.Bandera == true).ToList<PolizaExcelDM>();
                List<PolizaExcelDM> PolizasNO = listapoliza.Where(apn => apn.Bandera == false && apn.Mesaje != "BADZIP" && apn.Mesaje != "ARCHIVOREGISTRADO").ToList<PolizaExcelDM>();
                List<PolizaExcelDM> PolizasNoZIP = listapoliza.Where(apnz => apnz.Bandera == false && apnz.Mesaje == "BADZIP").ToList<PolizaExcelDM>();
                List<PolizaExcelDM> PolizasRegistradas = listapoliza.Where(apnz => apnz.Bandera == false && apnz.Mesaje == "ARCHIVOREGISTRADO").ToList<PolizaExcelDM>();


                // Mandar los incorrectos actuales en lo que se trabaja con el segundo filtro
                Console.WriteLine("Enviando Email con error primer filtro");
                Task tEmailEnviaError = new Task(() => control.CreaDetalleEmailError(DateTime.Now.ToShortTimeString(), PolizasNO));
                tEmailEnviaError.Start();

                List<PolizaExcelDM> listaFiltroInconrrectas = control.CreaPreregistroPolizasXLS(ref PolizasOK, dtPolizaRegistradas, dtEmpresaDetalles, dtPolizaExcluye, 0);
                List<PolizaExcelDM> listaFiltroInconrrectas2 = control.CreaPreregistroPolizasXLS(ref PolizasNoZIP, dtPolizaRegistradas, dtEmpresaDetalles, dtPolizaExcluye, 1);

                tEmailEnviaError.Wait();

                PolizasOK = PolizasOK.Where(al => al.Bandera == true).OrderBy(al => al.Compania).ToList<PolizaExcelDM>();
                PolizasNoZIP = PolizasNoZIP.Where(apnz => apnz.Bandera == true).ToList<PolizaExcelDM>();
                PolizasOK = PolizasOK.Concat(PolizasNoZIP).ToList();

                listaFiltroInconrrectas = listaFiltroInconrrectas.Concat(listaFiltroInconrrectas2).ToList();
                //PolizasNO = listapoliza.Where(al => al.Bandera == false).ToList<PolizaExcelDM>();

                Console.Clear();
                Console.WriteLine("Enviando Email correctos e incorrectos");

                Task tEmailEnviaPolizasCorrectas = new Task(() => control.CreaDetalleEmailOK(sTiempoInicio, DateTime.Now.ToShortTimeString(), PolizasOK));
                tEmailEnviaPolizasCorrectas.Start();


                Task tareaEmailResumen = new Task(() => control.CreaDetalleEmailResumen(PolizasOK.Count(), listaFiltroInconrrectas.Count(), PolizasNoZIP.Count(), listaFiltroInconrrectas2.Count(), PolizasRegistradas.Count()));
                tareaEmailResumen.Start();

                Task tareaEmailErrorFiltro = new Task(() => control.CreaDetalleEmailError(DateTime.Now.ToShortTimeString(), listaFiltroInconrrectas));
                tareaEmailErrorFiltro.Start();

                Task tareaEmailPolizasRegistradas = new Task(() => control.CreaDetalleEmailyaRegistradas(sTiempoInicio, DateTime.Now.ToShortTimeString(), listapolizaRegistradas));
                tareaEmailPolizasRegistradas.Start();

                tEmailEnviaPolizasCorrectas.Wait();
                tareaEmailResumen.Wait();
                tareaEmailErrorFiltro.Wait();
                tareaEmailPolizasRegistradas.Wait();
                Console.Clear();
                Console.WriteLine("Email Enviados");
                Console.Clear();
                Console.WriteLine("Iniciando eliminación de archivos");
                control.BorraArchivoDirectorio(@sRutaOrigen, 2);
                control.BorraArchivoDirectorio(@sRutaOrigen, 2);
                control.BorraArchivoDirectorio(sRutaOrigenTMP, 2);
                new PolizasController().lfDropBoxShareconfig(1);
                cronometro.Stop();
                Console.WriteLine("Duración (Minutos )" + cronometro.Elapsed.Minutes);
                Console.WriteLine(DateTime.Now);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("================================================");
                Console.WriteLine(" MISSION COMPLETE !");

                Console.WriteLine("================================================");
                Console.ResetColor();
                //Thread.Sleep(TimeSpan.FromMinutes(1).Milliseconds);
                //Console.ReadLine();
            }
            return true;
        }

        static bool lfMainCMAS()
        {
            try
            {
                ICmas _cmasrepository = new Cmas();
                return _cmasrepository.getDesynchronized();
                //return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
